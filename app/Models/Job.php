<?php

namespace App\Models;
use \Illuminate\Database\Eloquent\Model;


/**
 * Created by PhpStorm.
 * User: rockwith
 * Date: 15.05.17
 * Time: 11:23
 */
class Job extends Model
{

    protected $fillable = ['from', 'to', 'text', 'aggregator'];

    protected $hidden = [];


    public function messages()
    {
        return $this->hasMany(Message::class);
    }








}