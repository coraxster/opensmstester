<?php

namespace App\Models;
use \Illuminate\Database\Eloquent\Model;


/**
 * Created by PhpStorm.
 * User: rockwith
 * Date: 15.05.17
 * Time: 11:23
 */
class Message extends Model
{

    protected $fillable = ['from', 'to', 'text', 'date_sent', 'date_received', 'job_id', 'receiver'];


    protected $hidden = [];


    public function job()
    {
        return $this->belongsTo(Job::class);
    }








}