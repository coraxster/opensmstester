<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MessageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function messages(Request $request)
    {
        $pipe =  Message::query();

        if ($request->has('receiver')){
            $pipe = $pipe->where('receiver', $request->get('receiver'));
        }
        if ($request->has('from')){
            $pipe = $pipe->where('from', $request->get('from'));
        }
        if ($request->has('to')){
            $pipe = $pipe->where('to', $request->get('to'));
        }
        if ($request->has('sent_from')){
            $pipe = $pipe->where('date_sent', '>=', $request->get('sent_from'));
        }
        if ($request->has('job_id')){
            $pipe = $pipe->where('job_id', $request->get('job_id'));
        }

        return $pipe->paginate();
    }


    public function message($message_id)
    {
        return Message::with('job')->find($message_id);
    }

}
