<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class JobController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function jobs(Request $request)
    {
        $pipe =  Message::query();

        if($request->has('status')){
            $pipe = $pipe->where('status', $request->get('status'));
        }
        if ($request->has('aggregator')){
            $pipe = $pipe->where('aggregator', $request->get('aggregator'));
        }
        if ($request->has('from')){
            $pipe = $pipe->where('from', $request->get('from'));
        }
        if ($request->has('to')){
            $pipe = $pipe->where('to', $request->get('to'));
        }

        return $pipe->paginate();
    }


    public function job($job_id)
    {
        return Job::with('messages')->find($job_id);
    }

}
