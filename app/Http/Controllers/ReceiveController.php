<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\Message;
use Illuminate\Http\Request;

class ReceiveController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function receivedMessage(Request $request)
    {
        $job = Job::where('text', $request->get('text', 'nope'))
            ->where('from', $request->get('from_number', 'nope'))
            ->first();

        $message = Message::create(
            [
                'receiver' => $request->get('receiver', ''),
                'from' => $request->get('from_number', 0),
                'to' => $request->get('to', 0),
                'text' => $request->get('text', ''),
                'date_sent' => $request->get('date_sent'),
                'date_received' => $request->get('date_received'),
                'job_id' => $job->id ?? NULL
            ]
        );


        return $message;
    }

}
