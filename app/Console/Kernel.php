<?php

namespace App\Console;

use App\Console\Commands\startTestCommand;
use App\Jobs\Job;
use GuzzleHttp\Client;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Log;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        //todo: определить, нужно ли как то помечать задания, давать им оценку. Если да, то в формате отчётов за период или как то в реальном времени.
        //нужно ли получать время фактической отправки у смсСервиса(наверно да) возможно даже стоит подписываться на обновления статусов

        $schedule->command(startTestCommand::class)->everyMinute();




    }
}
