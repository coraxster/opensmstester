<?php

namespace App\Console\Commands;

use App\Jobs\Job;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class startTestCommand extends Command
{

    protected $signature = 'test:start';
    protected $description = 'Create test job and send sms via smsService';



    public function handle(Client $client)
    {
        $job = Job::create(
            [
                'from' => 'OpenBroker',
                'to' => 79155052399,
                'text' => str_random(32),
                'aggregator' => 'any'
            ]
        );

        $body = [
            'messages' => [
                [
                    'phone' => $job->to,
                    'text' => $job->text
                ]
            ]
        ];
        $response = $client->post(config('app.sms_service_url') . '/v1/send',
            ['json' => $body, 'verify' => false]);
        Log::info('Sent sms via service', ['response' => $response]);
    }


}

