<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});


$app->post('/received_message', 'ReceiveController@receivedMessage');

$app->get('/messages', 'MessageController@messages');
$app->get('/message/{message_id}', 'MessageController@message');


$app->get('/jobs', 'JobController@jobs');
$app->get('/job/{job_id}', 'JobController@job');