<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('receiver');
            $table->string('from',38);
            $table->string('to', 38);
            $table->string('text');
            $table->timestamp('date_sent');
            $table->timestamp('date_received');
            $table->timestamps();
            $table->integer('job_id')->unsigned()->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
