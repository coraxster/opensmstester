#!/usr/bin/env python

#http://smstools3.kekekasvi.com/index.php?p=eventhandler

import fileinput, sys, os, requests, json

if sys.argv[1] != 'RECEIVED':
    exit

lines = []

for line in fileinput.input(sys.argv[2]):
    lines.append(line)
#os.remove(sys.argv[2])

from_number = lines[0][6:-1]
date_sent = lines[3][6:-1]
date_received = lines[4][10:-1]
text = lines[13:]


url = 'http://192.168.1.36/post'

payload = {'from_number': from_number, 'date_sent': date_sent, 'date_received' : date_received, 'text' : text}

requests.post(url, data=json.dumps(payload))